#Create a 2D array with 1 on the border and 0 inside
import numpy as np
b=np.ones([4,5])
b[1:3, 1:-1]=0
print('A 2D array with 1 on the border and 0 inside is \n',b)
