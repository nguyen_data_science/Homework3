# Create a random vector of size 30 and find the mean value
import numpy as np
a=np.random.randn(30)
print('The random vector a of size 30 is\n',a)
print('The mean value of array a is',a.sum()/30)