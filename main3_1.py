#Given a 1D array, negate all elements which are between
# 3 and 8, in place(not created a new array)
import numpy as np
array_1d=np.random.randint(0,12,8)
print('a 1D array is\n',array_1d)
print('all elements which are between 3 and 8 are\n',array_1d[(array_1d>3)&(array_1d<8)])