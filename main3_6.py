#Find indices of non-zero elements from the array [1,2,0,0,4,0]
import numpy as np
a=np.array([1,2,0,0,4,0])
b=np.where(a!=0)
print('The indices of non-zero elements from a are \n',b)