#Given an array x of 20 integers in the range (0, 100)
#and an random float in the range (0, 20)
#Find the index of x where the value at that index is closest to y.

import numpy as np
x = np.random.randint(0, 100, 20)
print('an array x of 20 integers in the range (0, 100) is',x)
y = np.random.uniform(0, 20)
print('an random float in the range (0, 20) is', y)
z=abs(x-y)
print('the index of x where the value at that index is closest to y is ',z.argmin())